   docker build -t 49.232.216.15:8000/yuchi/quake-gw:v1 .
   #上传docker仓库
   docker push 49.232.216.15:8000/yuchi/quake-gw:v1
  #删除本地docker镜像
  docker rmi 49.232.216.15:8000/yuchi/quake-gw:v1
  #部署yaml控制器 
  kubectl apply -f k8s-quake-gw-pods-svc.yaml
  #查看k8s容器状态
  kubectl get pods
  kubectl get svc