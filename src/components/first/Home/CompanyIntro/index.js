import styles from './index.less';
import ChIntro from '../ChIntro';
import EnIntro from '../EnIntro';
import { useEffect, useState, useRef } from 'react';
import { queryData } from '@/services/news';
import { connect } from 'umi';
// 视频插件
import 'video.js/dist/video-js.min.css';
import videojs from 'video.js';
const companyIntro = (props) => {
  const { user } = props;
  const videoRef = useRef();
  const [params, setParams] = useState({
    db: 'test01',
    table: 'userLayoutValue',
    filter: {
      user,
      style: window.localStorage.getItem('style'),
      name: 'companyIntro',
    },
    page: 1,
    size: 1,
  });
  const [companyIntro, setCompanyIntro] = useState({
    ch_intro: '',
    en_intro: '',
    videoUrl: 'http://www.w3school.com.cn/example/html5/mov_bbb.mp4',
  });
  var myVidero = '';
  useEffect(() => {
    queryCompanyIntro();
    myVidero = videojs('myVideo', {
      bigPlayButton: true,

      textTrackDisplay: false,

      posterImage: false,

      errorDisplay: false,
    });
    const playBtn = document.querySelector('.vjs-big-play-button');
    playBtn.innerHTML = '';
    playBtn.style.width = '80px';
    playBtn.style.height = '80px';
    playBtn.style.border = 'none';
    playBtn.style.backgroundColor = 'transparent';
    playBtn.style.transform = 'translateY(-15%)';
    playBtn.style.backgroundImage = `url(${require('@/assets/quakeWebsite/playbtn.png')})`;
    return () => myVidero.dispose();
  }, []);
  // 获取公司介绍信息
  const queryCompanyIntro = async () => {
    const res = await queryData(params);
    if (res.status_code === 1500) {
      if (res.data.length !== 0) {
        setCompanyIntro({ ...res.data[0].value, videoUrl: res.data[0].value.videoUrl[0].url });
        myVidero.src([{ type: 'video/mp4', src: res.data[0].value.videoUrl[0].url }]);
      }
    }
  };

  return (
    <div className={styles.companyIntro}>
      <div className={styles.introPhoto}>
        <img className={styles.mentu} src={require('@/assets/quakeWebsite/back ground.png')} />
        <video
          ref={videoRef}
          id="myVideo"
          controls
          preload="auto"
          data-setup="{}"
          poster={require('@/assets/quakeWebsite2/video.png')}
          className={`video-js vjs-default-skin vjs-big-play-centered ${styles.videoImg}`}
        >
          {/* <source id="source" src={companyIntro.videoUrl} type="video/mp4" /> */}
        </video>
      </div>
      <div className={styles.intro}>
        <div className={styles.title} style={{ textAlign: 'start', margin: 0 }}>
          <div className={styles.en}>about us</div>
          <div className={styles.ch}>
            公司介绍<span>Company Profile</span>
          </div>
        </div>
        <ChIntro width={'100%'}>{companyIntro.ch_intro}</ChIntro>
        <EnIntro width={'100%'}>{companyIntro.en_intro}</EnIntro>
        <button style={{ width: '220px', display: 'block' }} className={styles.currentButton}>
          了解更多
        </button>
      </div>
    </div>
  );
};
export default connect((props) => {
  return {
    user: props.user.user,
  };
})(companyIntro);
