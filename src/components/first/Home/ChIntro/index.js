import styles from './index.less'

export default(props)=>{
    const {width,children}=props

    return(<>
    <p style={{width:`${width}`}} className={styles.chComIntro}>
        {children}
    </p>
    </>)
}