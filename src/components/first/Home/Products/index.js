import Rtitle from '../Rtitle';
import styles from './index.less';
import { Row, Col } from 'antd';
import Enintro from '../EnIntro';
import { useEffect, useState } from 'react';
import { queryData, updateData } from '@/services/news';
import { connect } from 'umi';

const Products = (props) => {
  const { user } = props;
  const detail = () => {
    console.log('点击了');
  };
  const [products, setProducts] = useState([]);
  useEffect(() => {
    getProducts();
  }, []);

  // 获取产品信息
  const getProducts = async () => {
    const data = {
      db: 'test01',
      table: 'userLayoutValue',
      filter: {
        user,
        style: window.localStorage.getItem('style'),
        name: 'products',
      },
      page: 1,
      size: 1,
    };
    const newsData = await queryData(data);
    if (newsData.status_code === 1500) {
      if (newsData.data.length !== 0) {
        setProducts(newsData.data[0].value);
      }
    }
  };
  return (
    <div className={styles.Products} id="Products">
      <Rtitle
        location="start"
        enData="OurProducts"
        chData="产品推荐"
        topTitle="Products Recommended"
      />
      <div className={styles.products}>
        <Row justify="space-around" align="middle">
          {products.map((value, index) => {
            return (
              <Col sm={10} md={8} lg={6} xl={6} className={styles.proCard} key={value.uid}>
                <img src={value.coverImgURL} className={styles.bottomImg}></img>
                <div className={styles.intro}>
                  <div className={styles.productDetail}>
                    <span className={styles.title}>{value.title}</span>
                    <span className={styles.detail}>{value.intro}</span>
                  </div>
                  <button className={styles.learnMore}>+</button>
                </div>
              </Col>
            );
          })}
        </Row>
      </div>
      <Enintro width="733px">
        To become a leading premier IT solution provider, both effectivFor more products, please see
        more detailed cases
      </Enintro>
      <button
        style={{ width: '220px', display: 'block', margin: '40px auto 0' }}
        className={styles.currentButton}
      >
        查看更多
      </button>
    </div>
  );
};

export default connect((props) => {
  return {
    user: props.user.user,
  };
})(Products);
