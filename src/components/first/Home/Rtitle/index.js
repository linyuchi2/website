import styles from './index.less'
export default(props)=>{
    const {enData,chData,topTitle,location,num,color}=props
  return <div className={styles.title} style={{textAlign:location,margin:num}}>
            <div className={styles.en}>{topTitle}</div>
            <div className={styles.ch} style={{color:color}}>
              {chData}<span>{enData}</span>
            </div>
    </div>
}