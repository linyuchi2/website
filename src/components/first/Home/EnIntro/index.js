import styles from './index.less'

export default(props)=>{
    const {width,children,color}=props

    return(<>
    <div style={{width:`${width}`,color:color}} className={styles.enComIntro}>
        {children}
    </div>
    </>)
}