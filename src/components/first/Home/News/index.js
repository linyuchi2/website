import Rtitle from '../Rtitle';
import EnIntro from '../EnIntro';
import styles from './index.less';
import { useState, useEffect, useRef } from 'react';
import { queryData, updateData } from '@/services/news';
import { connect } from 'umi';
// 获取公司时态的信息
// import { queryNews } from '@/services/home';

const News = (props) => {
  const { user } = props;
  const content = useRef();
  const [news, setNews] = useState([]);
  const [num, setNum] = useState(1);
  const [newsList, setNewsList] = useState([]);
  let newsData = [];

  const getNews = async () => {
    const data = {
      db: 'test01',
      table: 'userLayoutValue',
      filter: {
        user,
        style: window.localStorage.getItem('style'),
        name: 'news',
      },
      page: 1,
      size: 1,
    };
    newsData = await queryData(data);
    if (newsData.status_code === 1500) {
      if (newsData.data.length !== 0) {
        setNewsList(newsData.data[0].value);
        setNews(newsData.data[0].value[0]);
      }
    }
  };

  useEffect(() => {
    getNews();
  }, []);
  const onChange = (index, e) => {
    e.stopPropagation();
    setNum(index + 1);
    setNews(newsList[index]);
    // content.current.innerHTML = newsList[index].content;
  };
  return (
    <div className={styles.News}>
      <Rtitle location="center" topTitle="Corporate tense" chData="公司时态" enData="Latest News" />
      <EnIntro width="653px">
        With stable and reliable product quality and good business reputation, we have won the trust
        of our customers to meet the new needs of our customers
      </EnIntro>
      <div className={styles.content}>
        <div className={styles.newsPhoto}>
          <img className={styles.dian} src={require('@/assets/quakeWebsite/dian.png')} />
          <img className={styles.photo} src={news.coverImgURL} />
        </div>
        <div className={styles.news}>
          <div className={styles.newsData}>
            <div className={styles.goMore}>
              <span>查看更多</span>
              <i className={`iconfont iconshuangyoujiantou- ${styles.moreIcon}`} />
            </div>
            <div className={styles.newsTitle}>
              <p className={styles.newsDate}>{news.create_time}</p>
              <p className={styles.newsTitleData}>{news.title}</p>
            </div>
            <div className={styles.newsIntro}>{news.intro}</div>
            {/*ref={content}*/}
          </div>
          <div className={styles.buzou}>
            <div className={styles.xian} />
            {newsList.map((item, index) => (
              <div
                className={styles.num}
                key={index}
                onClick={(e) => {
                  onChange(index, e);
                }}
              >
                {index + 1}
              </div>
            ))}
          </div>
        </div>
        <span className={styles.newsNum}>0{num}</span>
      </div>
    </div>
  );
};

export default connect((props) => {
  return {
    user: props.user.user,
  };
})(News);
