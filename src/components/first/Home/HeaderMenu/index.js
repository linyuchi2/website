import { useState, useEffect, useRef } from 'react';
import { Menu, Image } from 'antd';
import style from './menu.less';
import { connect, history } from 'umi';
import { queryData } from '@/services/news';
const { SubMenu } = Menu;

const HeaderMenu = (props) => {
  const { user } = props;
  const [current, handleCurrent] = useState();
  const [menu, setMenu] = useState([]);
  const [companyLogo, setLogo] = useState({});
  const mobileMenuRef = useRef();
  const headerRef = useRef();
  const handleClick = (e) => {
    history.push(e.key);
    handleCurrent(e.key);
  };
  useEffect(() => {
    queryValue();
  }, []);
  useEffect(() => {
    console.log(headerRef.current);
    handleCurrent(history.location.pathname);
    if (history.location.pathname === '/') {
      headerRef.current.style.background = 'transparent';
    } else {
      headerRef.current.style.background = '#0A0F21';
    }
  }, [history.location.pathname]);
  // 获取信息
  const queryValue = async () => {
    const data = {
      db: 'test01',
      table: 'userLayoutValue',
      filter: {
        name: 'logo',
        user,
        style: window.localStorage.getItem('style'),
      },
      page: 1,
      size: 10,
    };
    const logoData = await queryData(data);
    if (logoData.status_code === 1500) {
      if (logoData.data.length !== 0) {
        setLogo(logoData.data[0].value.companyLogo[0]);
      }
    }
    data.filter.name = 'headerMenu';
    const res = await queryData(data);
    if (res.status_code === 1500) {
      if (res.data.length !== 0) {
        setMenu(res.data[0].value);
      }
    }
  };
  const openMenu = () => {
    mobileMenuRef.current.children[1].style.right = '0';
    mobileMenuRef.current.children[0].style.right = '0';
  };
  const closeMenu = () => {
    mobileMenuRef.current.children[1].style.right = '-256px';
    mobileMenuRef.current.children[0].style.right = '-256px';
  };

  return (
    <div className={style.menu} ref={headerRef}>
      <img className={style.logo} width={140} height={50} src={companyLogo.url} />
      <Menu
        onClick={handleClick}
        className={style.menuUl}
        selectedKeys={[current]}
        mode="horizontal"
      >
        {menu.map((value) => {
          if (value.show === 'true') {
            if (value.routes) {
              return (
                <SubMenu key={value.path} className={style.menuList} title={value.name}>
                  {value.routes.map((element) => {
                    if (element.show === 'true') {
                      return (
                        <Menu.Item key={element.path} className={style.menuList}>
                          {element.name}
                        </Menu.Item>
                      );
                    } else {
                      return null;
                    }
                  })}
                </SubMenu>
              );
            }
            return (
              <Menu.Item key={value.path} className={style.menuList}>
                {value.name}
              </Menu.Item>
            );
          } else {
            return null;
          }
        })}
      </Menu>
      <i className={`iconfont iconcaidan ${style.icon}`} onClick={openMenu} />
      <div className={style.mobile} ref={mobileMenuRef}>
        <i className={`iconfont iconquxiao ${style.icon} ${style.close}`} onClick={closeMenu} />
        <Menu
          mode="inline"
          onClick={handleClick}
          selectedKeys={[current]}
          className={style.mobileMenu}
        >
          {menu.map((value) => {
            if (value.show === 'true') {
              if (value.routes) {
                return (
                  <SubMenu key={value.path} className={style.menuList} title={value.name}>
                    {value.routes.map((element) => {
                      if (element.show === 'true') {
                        return (
                          <Menu.Item key={element.path} className={style.menuList}>
                            {element.name}
                          </Menu.Item>
                        );
                      } else {
                        return null;
                      }
                    })}
                  </SubMenu>
                );
              }
              return (
                <Menu.Item key={value.path} className={style.menuList}>
                  {value.name}
                </Menu.Item>
              );
            } else {
              return null;
            }
          })}
        </Menu>
      </div>
    </div>
  );
};

export default connect((props) => {
  return {
    user: props.user.user,
  };
})(HeaderMenu);
