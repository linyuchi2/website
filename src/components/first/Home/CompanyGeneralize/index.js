import styles from './index.less';
import Rtitle from '../Rtitle';
import EnIntor from '../EnIntro';
import { queryData, updateData } from '@/services/news';
import { useEffect, useState } from 'react';
import { connect } from 'umi';
import { message } from 'antd';

const companyGeneralize = (props) => {
  const [companyData, setCompanyData] = useState([]);
  const { user, generalizeData } = props;
  useEffect(() => {
    queryGeneralize();
  }, []);
  // 获取概括信息
  const queryGeneralize = async () => {
    const data = {
      db: 'test01',
      table: 'userLayoutValue',
      filter: {
        user,
        style: window.localStorage.getItem('style'),
        name: 'companyGeneralize',
      },
      page: 1,
      size: 4,
    };
    const showRes = await queryData(data);
    if (showRes.status_code === 1500) {
      if (showRes.data.length !== 0) {
        setCompanyData(showRes.data[0].value);
        return;
      }
    }
    return;
  };

  return (
    <div className={styles.companygenneralize}>
      <Rtitle
        enData="About us"
        chData="公司的概括"
        topTitle="Company summary"
        location="center"
        color="#FFFFFF"
      />
      <EnIntor width="729px" color="#99A7CA">
        To become a leading premier IT solution provider, both effective and efficient in providing
        a comprehensive range of quality IT solution and digital marketing support.
      </EnIntor>
      <ul className={styles.guowang}>
        {companyData.map((value, index) => {
          return (
            <div key={index}>
              {index % 2 == 0 ? (
                <li className={styles.xianList} key={index}>
                  <div className={`${styles.dateRight} ${styles.date}`}>
                    <span className={styles.orange}>{value.month}</span>
                    <p>{value.year}</p>
                  </div>
                  <div className={styles.xian}>
                    {index + 1 == companyData.length ? (
                      <div className={styles.sanjiao}></div>
                    ) : null}
                  </div>
                  <div className={styles.dian}></div>
                  <div className={styles.zuoxian}></div>
                  <div className={`${styles.contentLeft} ${styles.content}`}>
                    <p className={styles.title}>{value.title}</p>
                    <p className={styles.main}>{value.content}</p>
                  </div>
                </li>
              ) : (
                <li className={styles.xianList} key={index}>
                  <div className={`${styles.dateLeft} ${styles.date}`}>
                    <span className={styles.orange}>{value.month}</span>
                    <p>{value.year}</p>
                  </div>
                  <div className={styles.xian}>
                    {index + 1 == companyData.length ? (
                      <div className={styles.sanjiao}></div>
                    ) : null}
                  </div>
                  <div className={styles.dian}></div>
                  <div className={styles.youxian}></div>
                  <div className={`${styles.contentRight} ${styles.content}`}>
                    <p className={styles.title}>{value.title}</p>
                    <p className={styles.main}>{value.content}</p>
                  </div>
                </li>
              )}
            </div>
          );
        })}
      </ul>
    </div>
  );
};

export default connect((props) => {
  return {
    user: props.user.user,
  };
})(companyGeneralize);
