import React, { useEffect, useState } from 'react';
import { Carousel } from 'antd';
import styles from './index.less';
import { queryData } from '@/services/news';
import { connect } from 'umi';

const MyBanner = (props) => {
  const { user } = props;
  const [banners, setBanners] = useState([]);
  useEffect(() => {
    queryBanner();
  }, []);
  const queryBanner = async () => {
    const data = {
      db: 'test01',
      table: 'userLayoutValue',
      filter: {
        user,
        style: 'first',
        name: 'banner',
      },
      page: 1,
      size: 10,
    };
    const res = await queryData(data);
    if (res.status_code === 1500) {
      setBanners(res.data);
      return;
    }
    return;
  };

  return (
    <div>
      <div className={styles.header}>
        <Carousel effect="fade" dots={true} autoplay={true}>
          {banners.length === 0
            ? null
            : banners.map((value) => {
                return (
                  <div key={value._id}>
                    <img className={styles.bannerImg} src={value.value.fileUrl} />
                  </div>
                );
              })}
        </Carousel>
      </div>
    </div>
  );
};

export default connect((props) => {
  return {
    user: props.user.user,
  };
})(MyBanner);
