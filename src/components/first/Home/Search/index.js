import styles from './index.less'

export default ()=>{
    return(
        <div className={styles.search}>
            <img className={styles.img} src={require('@/assets/quakeWebsite/memphis.png')}/>
            <div className={styles.searchIntro}>
                <p className={styles.big}>了解更多解决方案以获取更多信息</p>
                <p>The only list-building strategy you need for your business. Get it for free:</p>
            </div>
            <div className={styles.searchInput}>
                <input placeholder='搜索'/>
                <button>查询更多</button>
            </div>
        </div>
    )
}