import styles from './index.less';
import { useState, useEffect, useRef } from 'react';
const Content = () => {
  const [clickIndex, setClickIndex] = useState(0);
  const [anchorData, setAnchorData] = useState([
    {
      ref: 'moduleRef1',
      data: '主要功能介绍',
    },
    {
      ref: 'moduleRef2',
      data: '应用场景',
    },
    {
      ref: 'moduleRef3',
      data: '应用下载模块',
    },
    {
      ref: 'moduleRef4',
      data: '如何计算',
    },
  ]);

  const goAnchor = (index, ref) => {
    setClickIndex(index);
    console.log(ref);
    document.querySelector(`#${ref}`).scrollIntoView({
      behavior: 'smooth',
    });
  };
  return (
    <div className={styles.content}>
      <ul className={styles.anchor}>
        {anchorData.map((value, index) => {
          return (
            <li
              className={styles.anchorLi}
              key={index}
              onClick={() => {
                goAnchor(index, value.ref);
              }}
            >
              <p className={clickIndex === index ? styles.clickFont : null}>{value.data}</p>
              <p className={clickIndex === index ? styles.click : null}></p>
            </li>
          );
        })}
      </ul>
      <div className={styles.produc}>
        <div className={styles.intro}>
          <p className={styles.title} id="moduleRef1">
            {`{`}
            <span className={styles.titleContent}>主要功能介绍</span>
            {`}`}
          </p>
          <div className={styles.proInro}>
            <div className={styles.proIntroList}>
              <div className={styles.icon}>
                <i className={`iconfont icongexinghua ${styles.productIcon}`} />
                <div className={styles.kuai}></div>
              </div>
              <p className={styles.productTitle}>个性定制</p>
              <p className={styles.productIntro}>
                满足客户定制化需求，随时随地定制屏幕的内容和布局{' '}
              </p>
            </div>
            <div className={styles.proIntroList}>
              <div className={styles.icon}>
                <i className={`iconfont iconyuyin ${styles.productIcon}`} />
                <div className={styles.kuai}></div>
              </div>
              <p className={styles.productTitle}>AI合成技术</p>
              <p className={styles.productIntro}>使用业内先进AI技术，将文字合成语音播报 </p>
            </div>
            <div className={styles.proIntroList}>
              <div className={styles.icon}>
                <i className={`iconfont iconyun ${styles.productIcon}`} />
                <div className={styles.kuai}></div>
              </div>
              <p className={styles.productTitle}>海量云储存</p>
              <p className={styles.productIntro}>提供大量的视频、音频以供用户使用 </p>
            </div>
          </div>
        </div>

        <div className={styles.intro}>
          <p className={styles.title} id="moduleRef2">
            {`{`}
            <span className={styles.titleContent}>应用场景</span>
            {`}`}
          </p>
          <div className={styles.proInro}>
            <div className={styles.proIntroList}>
              <div className={styles.bigIcon}>
                <i className={`iconfont iconzhanhuiguanli ${styles.productIcon}`} />
              </div>
              <p className={styles.productTitle}>展会</p>
              <p className={styles.productIntro}>
                根据客户产品、业务为品牌策划定制专属微屏互动体验，吸引观众参观展位，提 升品牌曝光
              </p>
            </div>
            <div className={styles.proIntroList}>
              <div className={styles.bigIcon}>
                <i className={`iconfont iconshangchang ${styles.productIcon}`} />
              </div>
              <p className={styles.productTitle}>商城</p>
              <p className={styles.productIntro}>
                辅助品牌在线下营销场景中吸引大量人气，更好的连接消费者和品牌，促 进销售转化
              </p>
            </div>
            <div className={styles.proIntroList}>
              <div className={styles.bigIcon}>
                <i className={`iconfont icondiqiu ${styles.productIcon}`} />
              </div>
              <p className={styles.productTitle}>发布会</p>
              <p className={styles.productIntro}>辅助品牌更好的做宣传、推销</p>
            </div>
          </div>
        </div>
        <div className={styles.intro}>
          <p className={styles.title} id="moduleRef3">
            {`{`}
            <span className={styles.titleContent}>应用下载模块</span>
            {`}`}
          </p>
          <div className={styles.proInro}>
            <div className={styles.downloadList}>
              <div className={styles.logo}>
                <div className={styles.logoImg}>
                  <i className={`iconfont iconios ${styles.logos}`} />
                </div>
                <p className={styles.logoName}>IOS</p>
              </div>
              <button className={styles.downloadBtn}>IOS下载</button>
            </div>
            <div className={styles.downloadList}>
              <div className={styles.logo}>
                <div className={styles.logoImg}>
                  <i className={`iconfont iconandroid ${styles.logos}`} />
                </div>
                <p className={styles.logoName}>Android</p>
              </div>
              <button className={styles.downloadBtn}>Android下载</button>
            </div>
            <div className={styles.downloadList}>
              <div className={styles.logo}>
                <div className={styles.logoImg}>
                  <i className={`iconfont iconWeb ${styles.logos}`} />
                </div>
                <p className={styles.logoName}>Web</p>
              </div>
              <button className={styles.downloadBtn}>Web下载</button>
            </div>
            <div className={styles.downloadList}>
              <div className={styles.logo}>
                <div className={styles.logoImg}>
                  <i className={`iconfont iconSDK ${styles.logos}`} />
                </div>
                <p className={styles.logoName}>API接口</p>
              </div>
              <button className={styles.downloadBtn}>API接口下载</button>
            </div>
          </div>
        </div>

        <div className={styles.intro}>
          <p className={styles.title} id="moduleRef4">
            {`{`}
            <span className={styles.titleContent}>如何计费</span>
            {`}`}
          </p>
          <div className={styles.proInro}>
            <div className={styles.payList}>
              <div className={styles.payType}>
                <p className={styles.xian} />
                <p className={styles.payTitle}>套餐一</p>
                <p className={styles.littleIntro}>套餐一（付费）</p>
              </div>
              <div className={styles.payIntro}>
                <div className={styles.pay}>
                  <p>使用门店</p>
                  <p>1</p>
                </div>
                <div className={styles.pay}>
                  <p>有效期</p>
                  <p>1年</p>
                </div>
                <div className={styles.pay}>
                  <p>使用服务</p>
                  <p>申请购买</p>
                </div>
              </div>
              <div className={styles.payBtn}>
                <p>¥ 10000</p>
                <button className={styles.downloadBtn}>立即购买</button>
              </div>
            </div>
            <div className={styles.payList}>
              <div className={styles.payType}>
                <p className={styles.xian} />
                <p className={styles.payTitle}>套餐二</p>
                <p className={styles.littleIntro}>套餐二（付费）</p>
              </div>
              <div className={styles.payIntro}>
                <div className={styles.pay}>
                  <p>使用门店</p>
                  <p>2</p>
                </div>
                <div className={styles.pay}>
                  <p>有效期</p>
                  <p>1年</p>
                </div>
                <div className={styles.pay}>
                  <p>使用服务</p>
                  <p>申请购买</p>
                </div>
              </div>
              <div className={styles.payBtn}>
                <p>¥ 19000</p>
                <button className={styles.downloadBtn}>立即购买</button>
              </div>
            </div>
            <div className={styles.payList}>
              <div className={styles.payType}>
                <p className={styles.xian} />
                <p className={styles.payTitle}>套餐三</p>
                <p className={styles.littleIntro}>套餐三（付费）</p>
              </div>
              <div className={styles.payIntro}>
                <div className={styles.pay}>
                  <p>使用门店</p>
                  <p>4</p>
                </div>
                <div className={styles.pay}>
                  <p>有效期</p>
                  <p>1年</p>
                </div>
                <div className={styles.pay}>
                  <p>使用服务</p>
                  <p>申请购买</p>
                </div>
              </div>
              <div className={styles.payBtn}>
                <p>¥ 30000</p>
                <button className={styles.downloadBtn}>立即购买</button>
              </div>
            </div>
            <div className={styles.payList}>
              <div className={styles.payType}>
                <p className={styles.xian} />
                <p className={styles.payTitle}>套餐四</p>
                <p className={styles.littleIntro}>套餐四（付费）</p>
              </div>
              <div className={styles.payIntro}>
                <div className={styles.pay}>
                  <p>使用门店</p>
                  <p>10</p>
                </div>
                <div className={styles.pay}>
                  <p>有效期</p>
                  <p>1年</p>
                </div>
                <div className={styles.pay}>
                  <p>使用服务</p>
                  <p>申请购买</p>
                </div>
              </div>
              <div className={styles.payBtn}>
                <p>¥ 60000</p>
                <button className={styles.downloadBtn}>立即购买</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Content;
