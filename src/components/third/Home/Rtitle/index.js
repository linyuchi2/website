import styles from './index.less';
const Rtitle = (props) => {
  const { sTitle, bTitle, think } = props;
  return (
    <div className={styles.title}>
      <div className={styles.sTitle}>{sTitle}</div>
      <div className={styles.bTitle}>
        {bTitle}&nbsp;<strong>{think}</strong>
      </div>
    </div>
  );
};

export default Rtitle;
