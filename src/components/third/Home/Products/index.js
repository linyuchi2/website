import styles from './index.less';
import { Row, Col } from 'antd';
import MoreBtn from '../MoreBtn';
import { useState, useEffect } from 'react';
import { queryData } from '@/services/news';
import { connect } from 'umi';

const Products = (props) => {
  const { user } = props;
  const [products, setProducts] = useState([]);
  useEffect(() => {
    queryProducts();
  }, []);

  const queryProducts = async () => {
    const data = {
      db: 'test01',
      table: 'userLayoutValue',
      filter: {
        user,
        style: window.localStorage.getItem('style'),
        name: 'products',
      },
      page: 1,
      size: 1,
    };
    const newsData = await queryData(data);
    if (newsData.status_code === 1500) {
      if (newsData.data.length !== 0) {
        setProducts(newsData.data[0].value);
      }
    }
  };
  return (
    <div className={styles.products}>
      <div className={styles.title}>
        <div className={styles.sTitle}>Products Recommended</div>
        <div className={styles.bTitle}>
          RECOMMENDED&nbsp;<strong>BY QUAKE</strong>
        </div>
      </div>
      <div className={styles.products}>
        <Row justify="center" align="middle">
          {products.length === 0
            ? null
            : products.map((value) => (
                <Col sm={18} md={8} lg={6} xl={6} className={styles.proCard} key={value._id}>
                  <img src={value.coverImgURL} className={styles.bottomImg}></img>
                  <div className={styles.intro}>
                    <div className={styles.productDetail}>
                      <span className={styles.name}>{value.title}</span>
                    </div>
                    <button className={styles.learnMore}>See Detail</button>
                  </div>
                </Col>
              ))}
        </Row>
      </div>
      <MoreBtn />
    </div>
  );
};
export default connect((props) => {
  return {
    user: props.user.user,
  };
})(Products);
