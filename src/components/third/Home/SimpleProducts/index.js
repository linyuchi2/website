import { connect } from 'umi';
import styles from './index.less';
import { useState, useEffect, useImperativeHandle } from 'react';
import { queryData } from '@/services/news';

const SimpleProducts = (props) => {
  const { user, setType } = props;
  const [simpleProducts, setSimpleProducts] = useState([]);
  useEffect(() => {
    querySimpleProducts();
  }, []);
  useImperativeHandle(props.cRef, () => ({
    querySimpleProducts,
  }));
  const querySimpleProducts = async () => {
    const data = {
      db: 'test01',
      table: 'simpleProducts',
      filter: {
        user,
      },
      page: 1,
      size: 3,
    };
    const res = await queryData(data);
    if (res.status_code === 1500) {
      setSimpleProducts(res.data);
      return;
    }
    return;
  };
  return (
    <div
      id="simpleProducts"
      onClick={() => {
        setType('simpleProducts');
      }}
    >
      <div className={styles.title}>
        <div className={styles.sTitle}>Hot product recommendation</div>
        <div className={styles.bTitle}>
          THE FOLLOWING PRODUCT&nbsp;<strong>RECOMMENDATIONS</strong>
        </div>
      </div>
      <div
        className={styles.wire}
        style={{ width: 68, height: 2, background: '#56B5E1', margin: '35px auto 30px' }}
      ></div>
      <div className={styles.productsIconList}>
        {simpleProducts.length === 0
          ? null
          : simpleProducts.map((value) => (
              <div className={styles.productsIntro} key={value._id}>
                <img className={styles.productImg} src={value.fileUrl} />
                <span className={styles.productsName}>{value.title}</span>
              </div>
            ))}
      </div>
    </div>
  );
};

export default connect((props) => {
  return {
    user: props.user.user,
  };
})(SimpleProducts);
