import Rtitle from '../Rtitle';
import styles from './index.less';
// 视频插件
import 'video.js/dist/video-js.min.css';
import videojs from 'video.js';
import { queryData } from '@/services/news';
import { useEffect, useRef, useState } from 'react';
import { connect } from 'umi';
const CompanyIntro = (props) => {
  const { user } = props;
  const videoRef = useRef();
  const [params, setParams] = useState({
    db: 'test01',
    table: 'userLayoutValue',
    filter: {
      user,
      style: window.localStorage.getItem('style'),
      name: 'companyIntro',
    },
    page: 1,
    size: 1,
  });
  const [companyIntroValue, setCompanyIntro] = useState({
    ch_intro: '',
    en_intro: '',
    videoUrl: 'http://www.w3school.com.cn/example/html5/mov_bbb.mp4',
  });
  var myVidero = '';
  useEffect(() => {
    queryCompanyIntro();
    myVidero = videojs('myVideo', {
      bigPlayButton: true,

      textTrackDisplay: false,

      posterImage: false,

      errorDisplay: false,
    });
    const playBtn = document.querySelector('.vjs-big-play-button');
    playBtn.innerHTML = '';
    playBtn.style.width = '3em';
    playBtn.style.height = '3em';
    playBtn.style.border = 'none';
    playBtn.style.left = '80%';
    playBtn.style.backgroundColor = 'transparent';
    playBtn.style.backgroundImage = `url(${require('@/assets/quakeWebsite3/playbtn.png')})`;
    playBtn.style.transform = 'translateY(-20%)';
    playBtn.style.backgroundSize = 'contain';
  }, []);
  // 获取公司介绍信息
  const queryCompanyIntro = async (e) => {
    const res = await queryData(params);
    if (res.status_code === 1500) {
      if (res.data.length !== 0) {
        setCompanyIntro({ ...res.data[0].value, videoUrl: res.data[0].value.videoUrl[0].url });
        myVidero.src([{ type: 'video/mp4', src: res.data[0].value.videoUrl[0].url }]);
      }
    }
  };
  return (
    <div className={styles.companyIntro} id="CompanyIntro">
      <Rtitle sTitle="ABOUT US" bTitle="COMPANY" think="PROFILE" />
      <div className={styles.content}>{companyIntroValue.en_intro}</div>
      <div className={styles.companyIntroVideo}>
        <video
          ref={videoRef}
          id="myVideo"
          controls
          preload="auto"
          data-setup="{}"
          poster={require('@/assets/quakeWebsite3/companyInro.png')}
          className={`video-js vjs-default-skin vjs-big-play-centered ${styles.videoImg}`}
        >
          {/* <source id="source" src={url} type="video/mp4" /> */}
        </video>
      </div>
    </div>
  );
};

export default connect((props) => {
  return {
    user: props.user.user,
  };
})(CompanyIntro);
