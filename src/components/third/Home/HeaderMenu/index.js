import { useState, useRef } from 'react';
import { Menu, Image } from 'antd';
import style from './menu.less';
import logo from '@/assets/logo.png';
const { SubMenu } = Menu;

const HeaderMenu = () => {
  const [current, handleCurrent] = useState('home');
  const mobileMenuRef = useRef();
  const handleClick = (e) => {
    console.log('click ', e);
    handleCurrent(e.key);
  };

  const openMenu = () => {
    mobileMenuRef.current.children[1].style.right = '0';
    mobileMenuRef.current.children[0].style.right = '0';
  };
  const closeMenu = () => {
    mobileMenuRef.current.children[1].style.right = '-256px';
    mobileMenuRef.current.children[0].style.right = '-256px';
  };

  return (
    <div className={style.menu}>
      <img className={style.logo} width={140} height={50} src={logo} />
      <Menu
        onClick={handleClick}
        className={style.menuUl}
        selectedKeys={[current]}
        mode="horizontal"
      >
        <Menu.Item key="home" className={style.menuList}>
          HOME
        </Menu.Item>
        <Menu.Item key="solution" className={style.menuList}>
          SOLUTION
        </Menu.Item>
        <Menu.Item key="join" className={style.menuList}>
          HOW TO JOIN
        </Menu.Item>
        <Menu.Item key="serve" className={style.menuList}>
          SERVICE CENTRE
        </Menu.Item>
        <Menu.Item key="open" className={style.menuList}>
          QUAKE OPEN SOURCE
        </Menu.Item>
        <Menu.Item key="foreign" className={style.menuList}>
          FOREIGN CHANNELS
        </Menu.Item>
      </Menu>
      <i className={`iconfont iconcaidan ${style.icon}`} onClick={openMenu} />
      <div className={style.mobile} ref={mobileMenuRef}>
        <i
          className={`iconfont iconquxiao ${style.icon} ${style.close}`}
          style={{ position: 'absolute' }}
          onClick={closeMenu}
        />
        <Menu
          mode="inline"
          onClick={handleClick}
          selectedKeys={[current]}
          className={style.mobileMenu}
        >
          <Menu.Item key="home" className={style.menuList}>
            HOME
          </Menu.Item>
          <Menu.Item key="solution" className={style.menuList}>
            SOLUTION
          </Menu.Item>
          <Menu.Item key="join" className={style.menuList}>
            HOW TO JOIN
          </Menu.Item>
          <Menu.Item key="serve" className={style.menuList}>
            SERVICE CENTRE
          </Menu.Item>
          <Menu.Item key="open" className={style.menuList}>
            QUAKE OPEN SOURCE
          </Menu.Item>
          <Menu.Item key="foreign" className={style.menuList}>
            FOREIGN CHANNELS
          </Menu.Item>
        </Menu>
      </div>
    </div>
  );
};

export default HeaderMenu;
