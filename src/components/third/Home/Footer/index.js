import styles from './index.less';
import { useState, useEffect } from 'react';
import { connect } from 'umi';
import { queryData } from '@/services/news';
import { message } from 'antd';
const Footer = (props) => {
  const { user } = props;
  const [footer, handleFooter] = useState({
    companyIntro: '',
    companyLogo: [],
    companyAddress: '',
    companyPhone: '',
    companyEmail: '',
  });
  useEffect(() => {
    getFooter();
  }, []);
  // 获取底部栏信息
  const getFooter = async () => {
    const data = {
      db: 'test01',
      table: 'userLayoutValue',
      filter: {
        user,
        style: window.localStorage.getItem('style'),
        name: 'footerData',
      },
      page: 1,
      size: 1,
    };
    const res = await queryData(data);
    if (res.status_code === 1500) {
      if (res.data.length !== 0) {
        handleFooter(res.data[0].value);
      }
    }
  };

  return (
    <div className={styles.footer} id="Footer">
      <div className={styles.footerContent}>
        <div className={styles.intro}>
          <img src={footer.companyLogo.length !== 0 ? footer.companyLogo[0].url : null} />
          <span>{footer.companyIntro}</span>
        </div>
        <div className={styles.footerLink}>
          <span>快速链接</span>
          <ul className={styles.linkList}>
            <li>关于我们</li>
            <li>荣耀与成就</li>
            <li>FQA</li>
          </ul>
        </div>
        <div className={styles.address}>
          <p />
          <div>
            <i className={`iconfont icondizhi ${styles.icon}`} />
            <span>地址：{footer.companyAddress}</span>
          </div>
          <div>
            <i className={`iconfont icondianhua ${styles.icon}`} />
            <span>电话：{footer.companyPhone}</span>
          </div>
          <div>
            <i className={`iconfont iconyouxiang ${styles.icon}`} />
            <span>邮箱：{footer.companyEmail}</span>
          </div>
        </div>
      </div>
      <div className={styles.footers}>
        Copyright © 2019 <span>Zahar</span>. All Rights Reserved.
      </div>
    </div>
  );
};

export default connect((props) => {
  return {
    user: props.user.user,
  };
})(Footer);
