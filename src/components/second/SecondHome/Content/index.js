import styles from './index.less';
import Rtitle from '../Components/rtitle/index';
import { useState, useEffect, useRef } from 'react';
const Content = () => {
  const [clickIndex, setClickIndex] = useState(0);
  const [anchorData, setAnchorData] = useState([
    {
      ref: 'moduleRef1',
      data: '主要功能介绍',
    },
    {
      ref: 'moduleRef2',
      data: '应用场景',
    },
    {
      ref: 'moduleRef3',
      data: '应用下载模块',
    },
    {
      ref: 'moduleRef4',
      data: '如何计算',
    },
  ]);
  const [clickAppSence, setClickAppSence] = useState(0);
  const [clickAppData, setClickAppData] = useState({
    name: '展会',
    intro: '根据客户产品、业务为品牌策划定制专属微屏互动体验，吸引观众参观展位，提升品牌曝光',
    imgUrl: '@/assets/quakeWebsite2/appSence.png',
  });
  const appScence = [
    {
      name: '展会',
      intro: '根据客户产品、业务为品牌策划定制专属微屏互动体验，吸引观众参观展位，提升品牌曝光',
      imgUrl: '@/assets/quakeWebsite2/appSence.png',
    },
    {
      name: '商场',
      intro: ' 辅助品牌在线下营销场景中吸引大量人气，更好的连接消费者和品牌，促进销售转化',
      imgUrl: '@/assets/quakeWebsite2/appSence.png',
    },
    {
      name: '发布会',
      intro: '辅助品牌更好的做宣传、推销',
      imgUrl: '@/assets/quakeWebsite2/appSence.png',
    },
  ];
  const goAnchor = (index, ref) => {
    setClickIndex(index);
    document.querySelector(`#${ref}`).scrollIntoView({
      behavior: 'smooth',
    });
  };
  return (
    <div className={styles.content}>
      <ul className={styles.anchor}>
        {anchorData.map((value, index) => {
          return (
            <li
              className={clickIndex === index ? styles.clickLi : styles.anchorLi}
              key={index}
              onClick={() => {
                goAnchor(index, value.ref);
              }}
            >
              <p>{value.data}</p>
            </li>
          );
        })}
      </ul>
      <div className={styles.produc}>
        {/* 功能介绍模块 */}
        <div className={`${styles.intro} ${styles.gray}`} id="moduleRef1">
          <Rtitle title="主要功能介绍" />
          <div className={styles.proInro}>
            <div className={styles.proIntroList}>
              <div className={styles.icon} style={{ background: ' #BA97FF' }}>
                <img className={styles.proImg} src={require('@/assets/quakeWebsite2/pro1.png')} />
              </div>
              <div className={styles.proName}>
                <p className={styles.productTitle}>个性定制</p>
                <p className={styles.productIntro}>
                  满足客户定制化需求，随时随地定制屏幕的内容和布局{' '}
                </p>
              </div>
            </div>
            <div className={styles.proIntroList}>
              <div className={styles.icon} style={{ background: ' #FF9190' }}>
                <img className={styles.proImg} src={require('@/assets/quakeWebsite2/pro2.png')} />
              </div>
              <div className={styles.proName}>
                <p className={styles.productTitle}>AI合成技术</p>
                <p className={styles.productIntro}>使用业内先进AI技术，将文字合成语音播报 </p>
              </div>
            </div>
            <div className={styles.proIntroList}>
              <div className={styles.icon} style={{ background: ' #72A2F6' }}>
                <img className={styles.proImg} src={require('@/assets/quakeWebsite2/pro3.png')} />
              </div>
              <div className={styles.proName}>
                <p className={styles.productTitle}>海量云储存</p>
                <p className={styles.productIntro}>提供大量的视频、音频以供用户使用 </p>
              </div>
            </div>
          </div>
        </div>
        {/* 应用场景模块 */}
        <div className={styles.intro} id="moduleRef2">
          <Rtitle title="应用场景" />
          <div className={styles.main}>
            <div className={styles.mainLeft}>
              {appScence.map((value, index) => {
                return (
                  <p
                    key={index}
                    className={clickAppSence === index ? styles.click_name : styles.name}
                  >
                    {value.name}
                  </p>
                );
              })}
            </div>
            <div className={styles.mainRight}>
              <i className={`iconfont iconzhanhuiguanli ${styles.appIcon}`} />
              <div className={styles.mainRightMain}>
                <div className={styles.main}>
                  <p className={styles.mainTitle}>{clickAppData.name}</p>
                  <p className={styles.mainInto}>{clickAppData.intro}</p>
                </div>
                <div className={styles.mainImg}>
                  <img src={require('@/assets/quakeWebsite2/appSence.png')} />
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* 应用下载模块 */}
        <div className={styles.intro} id="moduleRef3">
          <Rtitle title="应用下载模块" />
          <div className={styles.proInro}>
            <div className={styles.downloadList}>
              <div className={styles.logo}>
                <div className={styles.logoImg}>
                  <i className={`iconfont iconios ${styles.logos}`} />
                </div>
                <p className={styles.logoName}>IOS</p>
              </div>
              <button className={styles.downloadBtn}>IOS下载</button>
            </div>
            <div className={styles.downloadList}>
              <div className={styles.logo}>
                <div className={styles.logoImg}>
                  <i className={`iconfont iconandroid ${styles.logos}`} />
                </div>
                <p className={styles.logoName}>Android</p>
              </div>
              <button className={styles.downloadBtn}>Android下载</button>
            </div>
            <div className={styles.downloadList}>
              <div className={styles.logo}>
                <div className={styles.logoImg}>
                  <i className={`iconfont iconWeb ${styles.logos}`} />
                </div>
                <p className={styles.logoName}>Web</p>
              </div>
              <button className={styles.downloadBtn}>Web下载</button>
            </div>
            <div className={styles.downloadList}>
              <div className={styles.logo}>
                <div className={styles.logoImg}>
                  <i className={`iconfont iconSDK ${styles.logos}`} />
                </div>
                <p className={styles.logoName}>API接口</p>
              </div>
              <button className={styles.downloadBtn}>API接口下载</button>
            </div>
          </div>
        </div>
        {/* 计费模块 */}
        <div className={styles.intro} id="moduleRef4">
          <Rtitle title="如何计费" />
          <div className={styles.proInro}>
            <div className={styles.payList}>
              <div className={styles.payType}>
                <p className={styles.xian} />
                <p className={styles.payTitle}>套餐一</p>
                <p className={styles.littleIntro}>套餐一（付费）</p>
              </div>
              <div className={styles.payIntro}>
                <div className={styles.pay}>
                  <p>使用门店</p>
                  <p>1</p>
                </div>
                <div className={styles.pay}>
                  <p>有效期</p>
                  <p>1年</p>
                </div>
                <div className={styles.pay}>
                  <p>使用服务</p>
                  <p>申请购买</p>
                </div>
              </div>
              <div className={styles.payBtn}>
                <p>¥ 10000</p>
                <button className={styles.downloadBtn}>立即购买</button>
              </div>
            </div>
            <div className={styles.payList}>
              <div className={styles.payType}>
                <p className={styles.xian} />
                <p className={styles.payTitle}>套餐二</p>
                <p className={styles.littleIntro}>套餐二（付费）</p>
              </div>
              <div className={styles.payIntro}>
                <div className={styles.pay}>
                  <p>使用门店</p>
                  <p>2</p>
                </div>
                <div className={styles.pay}>
                  <p>有效期</p>
                  <p>1年</p>
                </div>
                <div className={styles.pay}>
                  <p>使用服务</p>
                  <p>申请购买</p>
                </div>
              </div>
              <div className={styles.payBtn}>
                <p>¥ 19000</p>
                <button className={styles.downloadBtn}>立即购买</button>
              </div>
            </div>
            <div className={styles.payList}>
              <div className={styles.payType}>
                <p className={styles.xian} />
                <p className={styles.payTitle}>套餐三</p>
                <p className={styles.littleIntro}>套餐三（付费）</p>
              </div>
              <div className={styles.payIntro}>
                <div className={styles.pay}>
                  <p>使用门店</p>
                  <p>4</p>
                </div>
                <div className={styles.pay}>
                  <p>有效期</p>
                  <p>1年</p>
                </div>
                <div className={styles.pay}>
                  <p>使用服务</p>
                  <p>申请购买</p>
                </div>
              </div>
              <div className={styles.payBtn}>
                <p>¥ 30000</p>
                <button className={styles.downloadBtn}>立即购买</button>
              </div>
            </div>
            <div className={styles.payList}>
              <div className={styles.payType}>
                <p className={styles.xian} />
                <p className={styles.payTitle}>套餐四</p>
                <p className={styles.littleIntro}>套餐四（付费）</p>
              </div>
              <div className={styles.payIntro}>
                <div className={styles.pay}>
                  <p>使用门店</p>
                  <p>10</p>
                </div>
                <div className={styles.pay}>
                  <p>有效期</p>
                  <p>1年</p>
                </div>
                <div className={styles.pay}>
                  <p>使用服务</p>
                  <p>申请购买</p>
                </div>
              </div>
              <div className={styles.payBtn}>
                <p>¥ 60000</p>
                <button className={styles.downloadBtn}>立即购买</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Content;
