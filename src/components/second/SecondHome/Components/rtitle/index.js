import styles from './index.less';

const Rtitle = ({ title }) => {
  return (
    <div className={styles.title}>
      <span className={styles.titleContent}>{title}</span>
      <p className={styles.xian}></p>
    </div>
  );
};
export default Rtitle;
