import styles from './index.less';
import Rtitle from '../Rtitle';
import { queryData } from '@/services/news';
import { useState, useEffect, useRef } from 'react';
import { connect } from 'umi';
// 视频插件
import 'video.js/dist/video-js.min.css';
import videojs from 'video.js';
const companyIntro = (props) => {
  const { user } = props;
  const videoRef = useRef();
  const [params, setParams] = useState({
    db: 'test01',
    table: 'userLayoutValue',
    filter: {
      user,
      style: window.localStorage.getItem('style'),
      name: 'companyIntro',
    },
    page: 1,
    size: 1,
  });
  const [companyIntro, setCompanyIntro] = useState({
    ch_intro: '',
    en_intro: '',
    videoUrl: '',
  });
  var myVidero = '';
  useEffect(() => {
    queryCompanyIntro();
    myVidero = videojs('myVideo', {
      bigPlayButton: true,

      textTrackDisplay: false,

      posterImage: false,

      errorDisplay: false,
    });
    const playBtn = document.querySelector('.vjs-big-play-button');
    playBtn.innerHTML = '';
    playBtn.style.width = '80px';
    playBtn.style.height = '80px';
    playBtn.style.border = 'none';
    playBtn.style.backgroundColor = 'transparent';
    playBtn.style.transform = 'translateY(-15%)';
    playBtn.style.backgroundImage = `url(${require('@/assets/quakeWebsite/playbtn.png')})`;
    return () => myVidero.dispose();
  }, []);
  // 获取公司介绍信息
  const queryCompanyIntro = async () => {
    const res = await queryData(params);
    if (res.status_code === 1500) {
      if (res.data.length !== 0) {
        setCompanyIntro({ ...res.data[0].value, videoUrl: res.data[0].value.videoUrl[0].url });
        myVidero.src([{ type: 'video/mp4', src: res.data[0].value.videoUrl[0].url }]);
      }
    }
  };

  return (
    <div id="CompanyIntro" id="CompanyIntro">
      <Rtitle ch="关于我们" en="About us" />
      <div className={styles.videoContent}>
        <div className={styles.videoBackground}>
          <div style={{ position: 'relative' }}>
            <video
              ref={videoRef}
              id="myVideo"
              controls
              preload="auto"
              data-setup="{}"
              poster={require('@/assets/quakeWebsite2/video.png')}
              className={`video-js vjs-default-skin vjs-big-play-centered ${styles.videlControl}`}
            >
              {/* <source id="source" src={companyIntro.videoUrl} type="video/mp4" /> */}
            </video>
          </div>
        </div>
        <div className={styles.order}>
          <p className={styles.comTitle}>公司介绍 Company Profile</p>
          <p className={styles.chComIntro}>{companyIntro.ch_intro}</p>
          <p className={styles.enComIntro}>{companyIntro.en_intro}</p>
        </div>
      </div>
      <img
        src={require('@/assets/quakeWebsite2/footer.png')}
        style={{ width: '100%', marginTop: '50px' }}
      ></img>
    </div>
  );
};
export default connect((props) => {
  return {
    user: props.user.user,
  };
})(companyIntro);
