import styles from './index.less'
export default(props)=>{
  const {ch,en} =props
  return <div className={styles.title} style={{textAlign:'center'}}>
            <div className={styles.en}>{en}</div>
            <div className={styles.ch}>
            {ch}
            </div>
    </div>
}