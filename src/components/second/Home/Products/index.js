import Rtitle from '../Rtitle';
import styles from './index.less';
import { Row, Col } from 'antd';
import { useEffect, useState } from 'react';
import { queryData } from '@/services/news';
import { connect } from 'umi';
const Products = (props) => {
  const { user } = props;
  const [products, setProducts] = useState([]);
  const detail = () => {
    console.log('点击了');
  };
  useEffect(() => {
    getProducts();
  }, []);

  // 获取产品信息
  const getProducts = async () => {
    const data = {
      db: 'test01',
      table: 'userLayoutValue',
      filter: {
        user,
        style: window.localStorage.getItem('style'),
        name: 'products',
      },
      page: 1,
      size: 1,
    };
    const newsData = await queryData(data);
    if (newsData.status_code === 1500) {
      if (newsData.data.length !== 0) {
        setProducts(newsData.data[0].value);
      }
    }
  };
  return (
    <div id="Products">
      <Rtitle ch="产品推荐" en="OurProducts" />
      <div className={styles.products}>
        <img
          className={styles.backImg}
          width={'90%'}
          src={require('@/assets/quakeWebsite2/memphis.png')}
          alt=""
        />
        <Row justify="center" align="middle">
          {products.length === 0
            ? null
            : products.map((value, index) => (
                <Col sm={10} md={8} lg={6} xl={6} className={styles.proCard} key={value.uid}>
                  <img src={value.coverImgURL} className={styles.bottomImg}></img>
                  <div className={styles.proIntro}>
                    <i className={`iconfont iconpindao ${styles.proIcon}`}></i>
                    <span className={styles.proName}>{value.title}</span>
                    <span className={styles.enName}>Quark microchannel</span>
                    <span className={styles.xian}></span>
                    <div className={styles.hideBox}>
                      <p className={styles.proIntros}>{value.intro}</p>
                      <button onClick={detail} className={styles.proDetailButton}>
                        查看详情
                      </button>
                    </div>
                  </div>
                </Col>
              ))}
        </Row>
      </div>
    </div>
  );
};

export default connect((props) => {
  return {
    user: props.user.user,
  };
})(Products);
