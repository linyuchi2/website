import styles from './index.less';
import { queryData, updateData } from '@/services/news';
import { useEffect, useState, useRef } from 'react';
import { Carousel } from 'antd';
import { connect } from 'umi';
const Banner = (props) => {
  const { user } = props;
  const [bannerData, setBannerData] = useState([]);
  const [bannerkey, setkey] = useState(0);
  const introRef = useRef();
  const bannerRef = useRef();
  const bannerChange = (from, to) => {
    setkey(to);
  };
  // 用户点击切换轮播图
  const goto = (key, e) => {
    e.stopPropagation();
    bannerRef.current.goTo(key);
  };
  useEffect(() => {
    getBannerArticle();
  }, []);
  const getBannerArticle = async () => {
    const data = {
      db: 'test01',
      table: 'userLayoutValue',
      filter: {
        user,
        style: 'second',
        name: 'bannerArticle',
      },
      page: 1,
      size: 10,
    };
    const res = await queryData(data);
    if (res.status_code === 1500) {
      if (res.data.length !== 0) {
        setBannerData(res.data[0].value);
      }
    }
  };
  return (
    <div className={styles.header} id="Banner">
      <Carousel ref={bannerRef} effect="fade" autoplay dots={false} beforeChange={bannerChange}>
        {bannerData.length === 0
          ? null
          : bannerData.map((value) => {
              return (
                <div key={value.uid}>
                  <img className={styles.bannerImg} src={value.coverImgURL} />
                </div>
              );
            })}
      </Carousel>
      <ul className={styles.bannerBottom} ref={introRef}>
        {bannerData.map((item, index) => (
          <li
            onClick={(e) => goto(index, e)}
            key={index}
            className={index === bannerkey ? styles.listActive : null}
          >
            <div className={styles.date}>{item.create_time}</div>
            <div className={styles.title}>{item.title}</div>
            <div className={styles.intro}>{item.intro}</div>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default connect((props) => {
  return {
    user: props.user.user,
  };
})(Banner);
