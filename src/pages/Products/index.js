import Banner from '@/components/PublicComponents/Banner';
import Nav from '@/components/PublicComponents/Nav';
import Content from '@/components/second/SecondHome/Content';
const Products = () => {
  return (
    <div style={{ marginTop: 80 }}>
      <Banner />
      <Nav />
      <Content />
    </div>
  );
};

export default Products;
