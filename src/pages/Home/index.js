import { useEffect, useState } from 'react';

import { queryData } from '@/services/news';
import { connect } from 'umi';
const Home = (props) => {
  const [userStyle, setUserStyle] = useState({});
  const [userLayout, setUserLayout] = useState([]);
  useEffect(() => {
    queryUserStyle();
  }, []);
  // 获取用户选择的样式
  const queryUserStyle = async () => {
    const data = {
      db: 'test01',
      table: 'userLayout',
      filter: {
        user: 'admin',
      },
      page: 1,
      size: 1,
    };
    const userData = await queryData(data);
    if (userData.status_code === 1500) {
      if (userData.data.length !== 0) {
        window.localStorage.setItem('style', userData.data[0].style);
        setUserStyle(userData.data[0]);
        setUserLayout(userData.data[0].layout);
      }
    }
  };
  // 动态获取组件
  const renderDetail = (name) => {
    const dynamicDetail = require(`@/components/${userStyle.style}/Home/${name}`).default;
    return dynamicDetail;
  };
  return (
    <div>
      {userLayout.map((value) => {
        if (value !== 'HeaderMenu' && value !== 'Footer') {
          const DynamicDetail = renderDetail(value);
          return <DynamicDetail key={value} />;
        }
      })}
    </div>
  );
};

export default connect((props) => {
  return {
    account: props.loginorQuery.account,
  };
})(Home);
