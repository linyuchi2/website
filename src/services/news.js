import request from '@/utils/newsRequest';

// 获取信息
export async function queryData(data) {
  return request('/mongodb_quertPage', {
    method: 'POST',
    data,
  });
}
