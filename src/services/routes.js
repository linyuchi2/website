import request from '@/utils/request';
export async function queryRoutes(data) {
  return request('/queryRoutes', {
    method: 'POST',
    data,
  });
}
export async function queryType() {
  return request('/queryType', {
    method: 'GET',
  });
}
