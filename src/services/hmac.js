import request from '@/utils/hmacRequest';

//获取hmac
export async function queryHmac(data) {
  return request('/createHmacLogin', {
    method: 'POST',
    data: {
      db: 'account',
      table: 'accounts',
      source: 'quake',
      ...data,
    },
  });
}
