import request from '@/utils/request';
export async function queryBanner() {
  return request('/getBanner');
}

export async function queryNews() {
  return request('/getNews');
}
