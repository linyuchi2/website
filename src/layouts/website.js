import NotFound from '@/pages/404';
import { useEffect, useState } from 'react';
import { queryHmac } from '@/services/hmac';
import { queryData } from '@/services/news';
import '@/assets/css/iconfont.css';
import { connect } from 'umi';
const Content = (props) => {
  const { children } = props;
  return <div style={{ minHeight: '100vh' }}>{children}</div>;
};

const Website = (props) => {
  const [userStyle, setUserStyle] = useState({});
  const [userLayout, setUserLayout] = useState([]);
  useEffect(() => {
    queryUserStyle();
  }, []);
  // 获取用户选择的样式
  const queryUserStyle = async () => {
    const { dispatch, account } = props;
    const res = await queryHmac(account);
    if (res.status_code === 1500) {
      dispatch({
        type: 'loginorQuery/handleHmac',
        payload: {
          hmac: res.hmac,
        },
      });
      const data = {
        db: 'test01',
        table: 'userLayout',
        filter: {
          user: 'admin',
        },
        page: 1,
        size: 1,
      };
      const userData = await queryData(data);
      if (userData.status_code === 1500) {
        if (userData.data.length !== 0) {
          window.localStorage.setItem('style', userData.data[0].style);
          setUserStyle(userData.data[0]);
          setUserLayout(userData.data[0].layout);
        }
      }
    }
  };
  // 动态获取组件
  const renderDetail = (name) => {
    const dynamicDetail = require(`@/components/${userStyle.style}/Home/${name}`).default;
    return dynamicDetail;
  };
  return (
    <div style={{ fontSize: 16, fontFamily: 'pingfang' }}>
      {userLayout.map((value) => {
        if (value === 'HeaderMenu') {
          const DynamicDetail = renderDetail(value);
          return <DynamicDetail key={value} />;
        }
      })}
      <Content>{props.children}</Content>
      {userLayout.map((value) => {
        if (value === 'Footer') {
          const DynamicDetail = renderDetail(value);
          return <DynamicDetail key={value} />;
        }
      })}
    </div>
  );
};

export default connect((props) => {
  return {
    account: props.loginorQuery.account,
  };
})(Website);
