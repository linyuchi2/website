/**
 * request 网络请求工具
 * 更详细的 api 文档: https://github.com/umijs/umi-request
 */
import { extend } from 'umi-request';
import { notification } from 'antd';
import { queryHmac } from '@/services/hmac';
import models from '@/models/loginorquery';
/**
 * 配置request请求时的默认参数
 */

const request = extend({
  prefix: process.env.BASE_API,
  // 默认错误处理
  credentials: 'include', // 默认请求是否带上cookie
});

request.interceptors.request.use((url, options) => {
  return {
    url,
    options,
  };
});

request.interceptors.response.use(async (response, options) => {
  return response;
});
export default request;
