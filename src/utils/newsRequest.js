/**
 * request 网络请求工具
 * 更详细的 api 文档: https://github.com/umijs/umi-request
 */
import { extend } from 'umi-request';
import { notification } from 'antd';
import { queryHmac } from '@/services/hmac';
import models from '@/models/loginorquery';
/**
 * 配置request请求时的默认参数
 */

const request = extend({
  prefix: process.env.GETNEWS_API,
  // 默认错误处理
  credentials: 'include', // 默认请求是否带上cookie
});

request.interceptors.request.use((url, options) => {
  // 将models/login文件内的state中的hmac对象插入到headers中
  // const headers = models.state.hmac;
  const headers = JSON.parse(window.localStorage.getItem('hmac'));
  return {
    url,
    options: { ...options, headers },
  };
});

request.interceptors.response.use(async (response, options) => {
  const loginDate = window.localStorage.getItem('date');
  const newDate = Date.now();
  if (response.url === `${process.env.LOGINHMAC_API}/createHmacLogin`) {
    return response;
  }
  if (loginDate - newDate < -(1000 * 60 * 7)) {
    // console.log('超过7分钟，重新获取hmac');
    window.localStorage.setItem('date', Date.now());
    const res = await queryHmac(models.state.account);
    window.localStorage.setItem('hmac', JSON.stringify(res.hmac));

    return response;
  }
  // console.log('小于7分钟不需要重新获取');
  return response;
});
export default request;
