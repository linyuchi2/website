const LoginorQuery = {
  namespace: 'loginorQuery',
  state: {
    account: {
      user: 'visitor',
      password: '123456',
    },
    hmac: {},
    firstDate: '',
  },
  reducers: {
    handleHmac(state, { payload }) {
      state.firstDate = Date.now();
      state.hmac = payload.hmac;
      window.localStorage.setItem('date', state.firstDate);
      window.localStorage.setItem('hmac', JSON.stringify(state.hmac));
      return { ...state };
    },
  },
};

export default LoginorQuery;
