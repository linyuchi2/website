import { queryData } from '@/services/news';
import { queryHmac } from '@/services/hmac';

let authRoutes = [];

function parseRoutes(value) {
  if (value !== []) {
    return value.map((item) => {
      if (item.show === 'true') {
        return {
          path: item.path,
          name: item.name,
          component: require(`@/pages/${item.component}`).default,
          exact: true,
        };
      } else {
        return false;
      }
    });
  }
  return [];
}

export function patchRoutes({ routes }) {
  parseRoutes(authRoutes).forEach((item) => routes[0].routes.push(item));
}

export async function render(oldRoutes) {
  const account = {
    user: 'visitor',
    password: '123456',
  };
  const res = await queryHmac(account);
  window.localStorage.setItem('hmac', JSON.stringify(res.hmac));
  const data = {
    db: 'test01',
    table: 'userLayout',
    filter: {
      user: 'admin',
    },
    page: 1,
    size: 1,
  };
  const userData = await queryData(data);
  if (userData.status_code === 1500) {
    if (userData.data.length !== 0) {
      window.localStorage.setItem('style', userData.data[0].style);
      const data = {
        db: 'test01',
        table: 'userLayoutValue',
        filter: {
          name: 'headerMenu',
          user: 'admin',
          style: userData.data[0].style,
        },
        page: 1,
        size: 10,
      };
      const route = await queryData(data);
      authRoutes = route.data[0].value;
    }
  }
  oldRoutes();
}
