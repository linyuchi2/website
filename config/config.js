// https://umijs.org/config/
import { defineConfig } from 'umi';
import defaultSettings from './defaultSettings';
import proxy from './proxy';
import sysConfig from '../sysConfig';
const { REACT_APP_ENV, APP_TYPE, NODE_ENV } = process.env;
export default defineConfig({
  hash: true,
  antd: {},
  dva: {
    hmr: true,
  },
  define: {
    APP_TYPE: APP_TYPE || '',
    'process.env': {
      NODE_ENV,
      ...(NODE_ENV === 'development' ? sysConfig.development : sysConfig.production),
    },
  },
  locale: {
    // default zh-CN
    default: 'zh-CN',
    antd: true,
    // default true, when it is true, will use `navigator.language` overwrite default
    baseNavigator: true,
  },
  dynamicImport: {
    loading: '@/components/PageLoading/index',
  },
  targets: {
    ie: 11,
  },
  // umi routes: https://umijs.org/docs/routing
  routes: [
    {
      path: '/',
      name: 'website',
      component: '../layouts/website',
      routes: [],
    },
    {
      path: '*',
      redirect: '/',
    },
    {
      component: './404',
    },
  ],
  // Theme for antd: https://ant.design/docs/react/customize-theme-cn
  theme: {
    // ...darkTheme,
    'primary-color': defaultSettings.primaryColor,
  },
  // @ts-ignore
  title: false,
  ignoreMomentLocale: true,
  proxy: proxy[REACT_APP_ENV || 'dev'],
  manifest: {
    basePath: '/',
  },
  history: { type: 'hash' },
  base: '/', //定义路由的基本路径
  publicPath: './', //定义资源的基本路径
});
