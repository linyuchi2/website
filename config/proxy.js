/**
 * 在生产环境 代理是无法生效的，所以这里没有生产环境的配置
 * The agent cannot take effect in the production environment
 * so there is no configuration of the production environment
 * For details, please see
 * https://pro.ant.design/docs/deploy
 */
export default {
  dev: {
    '/api/': {
      target: 'https://preview.pro.ant.design',
      changeOrigin: true,
      pathRewrite: {
        '^': '',
      },
    },
    // 获取信息
    '/getNews': {
      target: 'https://service-9fsxrgxy-1251342844.gz.apigw.tencentcs.com/test',
      changeOrigin: true,
      pathRewrite: {
        '^/getNews': '',
      },
    },
    // 获取登录hmac
    '/loginHmac': {
      target: 'https://service-qvid4fls-1251342844.gz.apigw.tencentcs.com/test',
      changeOrigin: true,
      pathRewrite: {
        '^/loginHmac': '',
      },
    },
  },
  test: {},
  pre: {},
};
