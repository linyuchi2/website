module.exports = {
  // 开发环境
  development: {
    BASE_API: '/api/',
    GETNEWS_API: '/getNews',
    LOGINHMAC_API: '/loginHmac',
  },
  // 生产环境
  production: {
    BASE_API: 'https://preview.pro.ant.design',
    GETNEWS_API: 'https://service-9fsxrgxy-1251342844.gz.apigw.tencentcs.com/release',
    LOGINHMAC_API: 'https://service-qvid4fls-1251342844.gz.apigw.tencentcs.com/release',
  },
};
