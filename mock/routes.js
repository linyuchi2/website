export default {
  'POST /api/queryRoutes': (req, res) => {
    if (req.body.type === 'first') {
      res.send([
        {
          path: '/',
          name: 'home',
          component: 'first/Home',
        },
      ]);
      return;
    }
    if (req.body.type === 'second') {
      res.send([
        {
          path: '/',
          name: 'home',
          component: 'Home/index',
        },
      ]);
      return;
    }
  },
  // 这个是获取用户选择
  'GET /api/queryType': {
    type: 'second',
  },
};
